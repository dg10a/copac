#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <set>
#include <experimental/filesystem>
#include <git2.h>
#include <nlohmann/json.hpp>
#include <curl/curl.h>

using json = nlohmann::json;
namespace fs = std::experimental::filesystem;

std::vector<std::string> comp_ops = {">=", "<=", "=", ">", "<"};

static std::string const aurjson_rpc_info("https://aur.archlinux.org/rpc/?v=5&type=info");
static std::string const aurjson_rpc_arg_prefix("&arg[]=");
static std::string const clone_url_base("https://aur.archlinux.org/");
static std::string const editor_env = "EDITOR";
static std::string const home_env = "HOME";
static int const url_char_limit = 2000;

struct Package {
    std::string name;
    bool aur = false;
    bool completed = false;
    bool makedep = true;
    bool explicitly = false;
    bool already_installed = false;
    std::string version;
    std::set<std::string> deps;
    std::set<std::string> makedeps;
};

std::tuple<std::string, std::string, std::string> split_package_version(std::string package_str)
{
    std::string package_name = package_str;
    std::string op;
    std::string version;
    std::size_t found;

    for (std::string comp_op : comp_ops) {
        found = package_str.find(comp_op);
        if (found != std::string::npos) {
            break;
        }
    }

    if (found != std::string::npos) {
        package_name = package_str.substr(0, found);
        op = package_str.substr(found, 2);
        version = package_str.substr(found + 2);
    }

    return std::make_tuple(package_name, op, version);
}

std::string create_cache_dir(void)
{
    std::string home_path(std::getenv(home_env.c_str()));
    std::string cache_dir_path(home_path + "/.cache/copac");
    if (!fs::exists(cache_dir_path)) {
        fs::create_directory(cache_dir_path);
    }
    return cache_dir_path;
}

std::string clone_package(std::string package_name, std::string clone_dir) {
    git_libgit2_init();

    std::string clone_url = clone_url_base + package_name + ".git";
    std::string clone_path = clone_dir + "/" + package_name;

    git_repository *repo = NULL;
    int error = git_clone(&repo, clone_url.c_str(), clone_path.c_str(), NULL);

    if (error < 0) {
        const git_error *e = giterr_last();
        printf("Error %d/%d: %s\n", error, e->klass, e->message);
        exit(error);
    }

    git_repository_free(repo);
    git_libgit2_shutdown();

    return clone_path;
}

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    size_t realsize = size * nmemb;
    ((std::string*)userdata)->append((char*)ptr, realsize);
    return realsize;
}

std::string download(std::string url)
{
    CURL *curl;
    CURLcode res;

    std::string response;

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        curl_easy_cleanup(curl);
    }
    return response;
}

json aurjson_info(std::set<std::string> package_names)
{
    json results = json::array();

    std::string download_url(aurjson_rpc_info);

    for (std::string package_name : package_names) {
        std::string query_str(aurjson_rpc_arg_prefix + package_name);
        if (download_url.size() + query_str.size() > url_char_limit) {
            json info = json::parse(download(download_url));
            for (json result : info["results"]) {
                results.push_back(result);
            }
            download_url = aurjson_rpc_info;
        } else {
            download_url.append(query_str);
        }
    }
    json info = json::parse(download(download_url));
    for (json result : info["results"]) {
        results.push_back(result);
    }

    return results;
}

void tsort_helper(std::string package_name, std::map<std::string, Package>& packages, std::set<std::string>& unmarked, std::map<std::string, int>& sorted, std::vector<std::string>& buildorder)
{
    if (sorted[package_name] == 2) {
        return;
    } else if (sorted[package_name] == 1) {
        // cycle
    }

    sorted[package_name] = 1;
    for (std::string dep_name : packages[package_name].deps) {
        if (packages.find(dep_name) != packages.end()) {
            tsort_helper(dep_name, packages, unmarked, sorted, buildorder);
        }
    }

    for (std::string makedep_name : packages[package_name].makedeps) {
        if (packages.find(makedep_name) != packages.end()) {
            tsort_helper(makedep_name, packages, unmarked, sorted, buildorder);
        }
    }

    sorted[package_name] = 2;
    unmarked.erase(unmarked.find(package_name));
    if (packages[package_name].aur == true) {
        buildorder.push_back(package_name);
    }
}

std::vector<std::string> tsort(std::map<std::string, Package>& packages)
{
    std::set<std::string> unmarked;
    std::map<std::string, int> sorted;
    std::vector<std::string> buildorder;

    for (auto it = packages.begin(); it != packages.end(); ++it) {
        std::string package_name = it->first;
        unmarked.insert(package_name);
        sorted[package_name] = 0;
    }

    while (unmarked.size() > 0) {
        tsort_helper(*(unmarked.begin()), packages, unmarked, sorted, buildorder);
    }
    return buildorder;
}

void update_packages(std::set<std::string> package_names, std::map<std::string, Package>& packages)
{
    while (package_names.size() > 0){
        std::set<std::string> new_package_names;

        json results = aurjson_info(package_names);

        for (json result : results) {
            std::string package_name = result["Name"];
            packages[package_name].aur = true;
            packages[package_name].version = result["Version"];
            if (packages[package_name].completed == true) {
                continue;
            }

            for (std::string dep_str : result["Depends"]) {
                auto split = split_package_version(dep_str);
                std::string dep_name = std::get<0>(split);
                packages[package_name].deps.insert(dep_name);
                if (packages.find(dep_name) == packages.end()) {
                    Package pkg;
                    pkg.name = dep_name;
                    packages[pkg.name] = pkg;
                    new_package_names.insert(dep_name);
                }
                packages[dep_name].makedep = false;
            }

            for (std::string makedep_str : result["MakeDepends"]) {
                auto split = split_package_version(makedep_str);
                std::string makedep_name = std::get<0>(split);
                packages[package_name].makedeps.insert(makedep_name);
                if (packages.find(makedep_name) == packages.end()) {
                    Package pkg;
                    pkg.name = makedep_name;
                    packages[pkg.name] = pkg;
                    new_package_names.insert(makedep_name);
                }
            }

            packages[package_name].completed = true;
        }
        package_names = new_package_names;
    }
}

// https://stackoverflow.com/a/478960
std::string run_command(std::string cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
        result += buffer.data();
    }
    return result;
}

std::tuple<std::string, std::string> parse_pacman_query(std::string result)
{
    int slash_idx = result.find("/");
    int space_idx = result.find(" ");
    int newline_idx = result.find("\n");
    std::string provider_name = result.substr(slash_idx + 1, space_idx - (slash_idx + 1));
    std::string version = result.substr(space_idx + 1, newline_idx - (space_idx + 1));
    return std::make_tuple(provider_name, version);
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cout << "No packages given." << std::endl;
        exit(1);
    }

    std::string cache_dir_path = create_cache_dir();

    std::map<std::string, Package> packages;
    std::set<std::string> package_names;

    for (int i = 1; i < argc; ++i) {
        std::string package_str = argv[i];
        auto split = split_package_version(package_str);
        Package pkg;
        pkg.name = std::get<0>(split);
        pkg.explicitly = true;
        pkg.makedep = false;
        packages[pkg.name] = pkg;
        package_names.insert(pkg.name);
    }

    update_packages(package_names, packages);
    std::vector<std::string> buildorder = tsort(packages);

    for (std::string package_name : buildorder) {
        // check if provider for this package is already installed
        std::string query_command("pacman -Qs " + package_name);
        std::string res = run_command(query_command);
        if (res.size() != 0) {
            auto split = parse_pacman_query(res);
            std::string provider_name = std::get<0>(split);
            packages[package_name].already_installed = true;
            std::cout << "Package \"" + package_name + "\" already provided by \"" + provider_name + "\"." << std::endl;
            continue;
        }

        std::string clone_path = clone_package(package_name, cache_dir_path);

        std::string editor_name(std::getenv(editor_env.c_str()));
        std::string editor_cmd(editor_name + " " + clone_path + "/" + "PKGBUILD");
        system(editor_cmd.c_str());

        std::string build_command("cd " + clone_path + " && makepkg -sri --noconfirm");
        if (packages[package_name].explicitly == false) {
            build_command.append(" --asdeps");
        }
        system(build_command.c_str());

        fs::remove_all(clone_path.c_str());
    }

    std::string build_deps;
    for (std::string package_name : buildorder) {
        if (packages[package_name].makedep == true && packages[package_name].already_installed == false) {
            build_deps.append(package_name + " ");
        }
    }

    if (build_deps.size() > 0) {
        std::cout << "Removing AUR build dependencies..." << std::endl;
        std::string remove_command("sudo pacman -Rns --noconfirm " + build_deps);
        system(remove_command.c_str());
    }
}
