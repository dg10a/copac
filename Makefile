copac: copac.cpp
	g++ -std=c++14 copac.cpp -lstdc++fs -lgit2 -lcurl -o copac

install: copac
	install -D -m755 copac "$(DESTDIR)$(PREFIX)/bin/copac"

clean:
	-rm -f copac
